#!/usr/bin/env python3

import re
from subprocess import Popen, PIPE, STDOUT
from elg import FlaskService
from elg.model import StructuredTextRequest
from elg.model import TextRequest
from elg.model import TextsResponse
from typing import Dict


'''
# command line script
./create_venv.sh
venv_elg_vmets/bin/python3 ./elg_sdk_synth.py --json='{"type": "text", "content": "terre tere ÜRO"}'
venv_elg_vmets/bin/python3 ./elg_sdk_synth.py --json='{"type":"structuredText", "texts":[{"content":"tere", "features":{"hint":"", "pos":"S", "features":"sg kom", "kigi":""}}]}'
venv_elg_vmets/bin/python3 ./elg_sdk_synth.py --json='{"params":{"cmdline":["--guess"]}, "type":"structuredText", "texts":[{"content":"palk", "features":{"hint":"", "pos":"S", "features":"sg kom", "kigi":""}}]}'
venv_elg_vmets/bin/python3 ./elg_sdk_synth.py --json='{"params":{"cmdline":["--addphonetics"]}, "type":"structuredText", "texts":[{"content":"palk", "features":{"hint":"palgi", "pos":"S", "features":"sg kom", "kigi":""}}]}'

# web server in docker & curl
docker build -t tilluteenused/vabamorf_synth:2022.08.15 .
docker login -u tilluteenused
docker run -p 7000:7000 tilluteenused/vabamorf_synth:2022.08.15

curl --silent --request POST --header "Content-Type: application/json" --data '{"type": "text", "content": "terre tere ÜRO"}' localhost:7000/process | jq
curl --silent --request POST --header "Content-Type: application/json" --data '{"type":"structuredText", "texts":[{"content":"tere", "features":{"hint":"", "pos":"S", "features":"sg kom", "kigi":""}}]}' localhost:7000/process | jq
curl --silent --request POST --header "Content-Type: application/json" --data '{"params":{"cmdline":["--guess"]}, "type":"structuredText", "texts":[{"content":"palk", "features":{"hint":"", "pos":"S", "features":"sg kom", "kigi":""}}]}' localhost:7000/proces | jq
curl --silent --request POST --header "Content-Type: application/json" --data '{"params":{"cmdline":["--addphonetics"]}, "type":"structuredText", "texts":[{"content":"palk", "features":{"hint":"palgi", "pos":"S", "features":"sg kom", "kigi":""}}]}' localhost:7000/process | jq
'''


class FILOSOFT_SYNTH4ELG(FlaskService):
    non_space_pattern = re.compile(r'\S+')

    def process_text(self, request) -> TextsResponse:
        '''
        Sünteesime loetletud sõnadest kõikvõimalikud vormid
        :param request: request.content tühikutega eraldatud sõnade loend
        :return: sünteesiprogrammi väljund
        '''
        text_in = request.content
        my_params = {"cmdline": []}  # run vmets with default flags
        if (request.params is not None) and ("cmdline" in request.params):
            my_params = request.params  # run vmets with given flags
        texts_out = self.qyery_text(text_in, my_params)
        return TextsResponse(texts=texts_out)

    def process_structured_text(self, request) -> TextsResponse:
        '''
        Sünteesime etteantud sõnadest etteantud vormid
        :param request: sõnade ja vormide loend
        :return: sünteesiprogrammi väljund
        '''
        my_params = {"cmdline": []}  # run vmets with default flags
        if (request.params is not None) and ("cmdline" in request.params):
            my_params = request.params  # run vmets with given flags
        texts_out = self.query_json(request.texts, my_params)
        return TextsResponse(texts=texts_out)

    def token_annotation(self, match):
        return {
            'content': match.group(),
            'features': {'hint': '', 'pos': '*', 'features': '*', 'kigi': ''}
        }

    def qyery_text(self, text, params):
        json_in = [self.token_annotation(m) for m in self.non_space_pattern.finditer(text)]
        lines_in = ''
        for t in json_in:
            lines_in += f'{t["content"]} //_{t["features"]["pos"]}_ {t["features"]["features"]}, //{t["features"]["kigi"]}\n'
        return self.query_vmets(lines_in, params)

    def query_json(self, q_json, params):
        lines_in = ''
        for q in q_json:
            if len(q.features["hint"]) > 0:
                lines_in += f'{q.content} ({q.features["hint"]}) //_{q.features["pos"]}_ {q.features["features"]}, //{q.features["kigi"]}\n'
            else:
                lines_in += f'{q.content} //_{q.features["pos"]}_ {q.features["features"]}, //{q.features["kigi"]}\n'
        return self.query_vmets(lines_in, params)

    def query_vmets(self, lines_in, params):
        path = '.'
        cmd = [path + '/vmets', '--path', path] + params["cmdline"]
        p = Popen(cmd, stdout=PIPE, stdin=PIPE, stderr=STDOUT)
        list_out = p.communicate(input=bytes(lines_in, 'utf-8'))[0].decode('utf-8').split('\n')
        '''
        $ echo "palk (palgi) //_S_ sg kom, //" | vmets
        palk (palgi) //_S_ sg kom, //     palgi+ga //_S_ sg kom, //
        $ echo -e "ÜRO //_*_ *, //\nNATs //_*_ *, //" | vmets
        ÜRO //_*_ *, //     ÜRO //_Y_ ?, //
        NATs //_*_ *, //     ####
        $ echo "palk (palgi) //_S_ sg kom, //ki" | vmets
        palk (palgi) //_S_ sg kom, // ki    palgi+gagi //_S_ sg kom, //

        '''
        texts = []
        for line in list_out:
            synts = line.split('    ')
            if len(synts) < 2:
                continue
            # synts[0] -- seda lemmat sünteesisime
            lemma = hint = pos = features = kigi = ''
            paring = synts[0].split('//')
            lemma_hint = paring[0].split(' ') # lemma ja võibla hint
            lemma = lemma_hint[0]
            if len(lemma_hint) > 1:
                hint = lemma_hint[1].strip('()')
              
            if len(paring[1]) > 3:
                pos = paring[1][1]
            if len(paring[1]) > 5:
                features = paring[1][4:].strip(', ')

            if paring[2] != '':
                kigi = paring[2]
 
            generated_forms = []        
            # synts[1:] -- sünteesitud vormid
            for synt in synts[1:]:
                if synt.strip() == '####':
                    continue
                synt = synt.split('//')
                generated_forms.append( {   "token": synt[0].strip(), 
                                            "pos": synt[1][1],
                                            "features": synt[1][4:].rstrip(', ')
                                        } )
            texts.append(   {   "content":lemma,
                                "features":
                                {   "hint": hint,
                                    "pos": pos,
                                    "features": features,
                                    "kigi": kigi,
                                    "generated_forms": generated_forms
                                }
                            }
                        )
        return texts


flask_service = FILOSOFT_SYNTH4ELG("Filosofti morfoloogiline süntesaator")
app = flask_service.app


def run_server() -> None:
    '''
    Run as flask webserver
    '''
    app.run()

def run_test(my_query_str: str) -> Dict:
    my_query = json.loads(my_query_str)
    if my_query["type"] == "text":
        return run_test_TextRequest(my_query)
    elif my_query["type"] == "structuredText":
        return run_test_StructuredTextRequest(my_query)


def run_test_StructuredTextRequest(my_query: Dict) -> Dict:
    '''
    Run as command line script
    :param my_query: input in json
    '''
    service = FILOSOFT_SYNTH4ELG("Filosofti morfoloogiline süntesaator")
    my_params = {"cmdline": []}  # run vmets with default flags
    if ("params" in my_query) and ("cmdline" in my_query["params"]):
        my_params = my_query["params"]  # run vmets with given flags
    my_request = StructuredTextRequest(texts=my_query["texts"], params=my_params)
    response = service.process_structured_text(request=my_request)

    response_json_str = response.json(exclude_unset=True)  # exclude_none=True
    response_json_json = json.loads(response_json_str)
    return response_json_json


def run_test_TextRequest(my_query: Dict) -> Dict:
    '''
    Run as command line script
    :param my_query: input in json
    '''
    service = FILOSOFT_SYNTH4ELG("Filosofti morfoloogiline süntesaator")
    my_params = {"cmdline": []}  # run vmets with default flags
    if ("params" in my_query) and ("cmdline" in my_query["params"]):
        my_params = my_query["params"]  # run vmets with given flags
    request = TextRequest(content=my_query["content"], params=my_params)
    response = service.process_text(request)

    response_json_str = response.json(exclude_unset=True)  # exclude_none=True
    response_json_json = json.loads(response_json_str)
    return response_json_json


if __name__ == '__main__':
    import argparse
    import json
    import sys

    argparser = argparse.ArgumentParser(allow_abbrev=False)
    argparser.add_argument('-j', '--json', type=str, help='sentences to precrocess')
    args = argparser.parse_args()
    if args.json is None:
        run_server()
    else:
        '''
        query_json_1 = \
            {
                "params": {"cmdline": ["--dontguess"]},
                "type": "structuredText",
                "texts":
                [
                    {
                        "content": "terre",
                        "features":
                        {
                            "hint": "",
                            "pos": "S",
                            "features": "pl p",
                            "kigi": "kigi"
                        }
                    },
                    {
                        "content": "ÜRO",
                        "features":
                            {
                                "hint": "",
                                "pos": "*",
                                "features": "*",
                                "kigi": ""
                            }
                    },
                    {
                        "content": "Hei",
                        "features":{"hint": "", "pos": "*", "features": "*", "kigi": "kigi"}
                    }
                ]
            }
        query_json_2 = \
            {
                "params": {"cmdline": ["--addphonetics"]},
                "type": "text",
                "content": "terre tere ÜRO palk"
            }

        query_json_3 = \
            {
                "params":{"cmdline":["--guess"]},
                "type":"structuredText",
                "texts":
                    [
                        {
                            "content":"palk",
                            "features":{"hint":"", "pos":"S", "features":"sg kom", "kigi":"kigi"}
                        }
                    ]
            }
        json.dump(run_test(json.dumps(query_json_3)), sys.stdout, indent=4)
        '''
        json.dump(run_test(args.json), sys.stdout, indent=4)
