# Container with a morphological generator for Estonian

[Filosoft morphological generator](https://github.com/Filosoft/vabamorf/blob/master/apps/cmdline/vmets/) container (docker) with
interface compliant with [ELG requirements](https://european-language-grid.readthedocs.io/en/release1.0.0/all/LTInternalAPI.html#).

## Contains  <a name="Contains"></a>

* [Filosoft morphological generator](https://github.com/Filosoft/vabamorf/blob/master/apps/cmdline/vmets/)
* Container and interface code

## Preliminaries

* You should have software for making / using the container installed; see instructions on the [docker web site](https://docs.docker.com/).
* In case you want to compile the code or build the container yourself, you should have version control software installed; see instructions on the [git web site](https://git-scm.com/).

## Downloading image from Docker Hub

You may dowload a ready-made container from Docker Hub, using the Linux command line (Windows / Mac command lines are similar):

```commandline
docker pull tilluteenused/vabamorf_synth:2022.08.15
```

Next, continue to the section [Starting the container](#Starting_the_container).

## Making your own container

### 1. Downloading the source code

<!---
Lähtekood koosneb 2 osast
1. json liides, veebiserver ja konteineri tegemise asjad
2. FSi morf sünteesi programm
---->

```commandline
mkdir -p ~/gitlab-docker-elg
cd ~/gitlab-docker-elg
git clone https://gitlab.com/tilluteenused/docker-elg-synth gitlab-docker-elg-synth
```

The repo contains a compiled [morphological generator](https://github.com/Filosoft/vabamorf/blob/master/apps/cmdline/vmets/) by Filosoft:

* **_vmets_** - programme for morphological generation
* **_et.dct_** - lexicon used by the generator 

In case you want to compile the programs (**_vmets_**) or change and re-assemble the lexicon (**_et.dct_**), follow the [instructions](https://github.com/Filosoft/vabamorf/blob/master/doc/make_programs_and_lexicons.md).

### 2. Building the container

```commandline
cd ~/gitlab-docker-elg/gitlab-docker-elg-synth
docker build -t tilluteenused/vabamorf_synth:2022.08.15 .
```

## Starting the container <a name="Starting the container"></a>

```commandline
docker run -p 7000:7000 tilluteenused/vabamorf_synth:2022.08.15
```

One need not be in a specific directory to start the container.

Ctrl+C in a terminal window with a running container in it will terminate the container.

## Query json

Note that the Python json library renders text in ASCII by default;
accented letters etc. are presented as Unicode codes, e.g. õ = \u00f5.

### Version 1. 

All the inflectional forms of the words will be generated.

```json
{
  "type":"text",
  "content": string, /* A list of LEMMAs, delimited by space. All the inflectional forms of every LEMMA will be generated */
}
```

### Variant 2.

Only the specified inflectional forms will be generated.

```json
{
  "params":{"cmdline": []}   /* FLAGS (optional) */
  "type": "structuredText",
  "texts": 
  [ { "content": string /* LEMMA to generate */
      "features": 
      {
        "hint": string,     /* HINT; empty by default */
        "pos": string,      /* POS; '*' denotes all possible POS */
        "features": string, /* Grammatical FEATURES; '*' denotes all possible FEATURE combinations */
        "kigi": string      /* clitic KI/GI; empty by default */
      }
    }
  ]
}
```

## Response json

For every word, the response contains also info about the query.

```json
{ "response":
  { "type":"texts",
    "texts": 
    [ { "content": string, /* in:LEMMA to generate */
        "features":
        { "hint": string,         /* in:HINT */
          "pos": string,          /* in:POS */
          "features": string,     /* in:FEATURES  */
          "kigi":string           /* in:clitic KI/GI */
          "generated_forms":        /* out:array of results */
          [ { "token": string,      /* out:generated wordform */
              "pos":string,         /* out:POS of the generated wordform */
              "features": string    /* out:Grammatical FEATURES of the generated wordform */
            }
          ]
        }
      }
    ]
  }
}    
```

The comments indicate how the JSON structure elements are related to the input and output of the [Filosoft morphological generator](https://github.com/Filosoft/vabamorf/blob/master/apps/cmdline/vmets/): params = FLAGS, content = LEMMA, hint = HINT, pos = POS, features = FEATURES

In addition, the structure element ```kigi``` denotes the clitic ki/gi:

* _"kigi": "kigi"_ - _ki_ or _gi_ should be appended to the word form (the choice depends on the final phone of the wordform)
* _"kigi": ""_ - do not append _ki_ or _gi_ 

## Usage examples

### Example 1.

All the inflectional forms of the words will be generated.

```commandline
curl -i --request POST --header "Content-Type: application/json" --data '{"type": "text", "content": "Terre ÜRO saadik."}' localhost:7000/process
```

```json
HTTP/1.1 200 OK
Server: gunicorn
Date: Mon, 15 Aug 2022 14:09:38 GMT
Connection: close
Content-Type: application/json
Content-Length: 3796

{"response":{"type":"texts","texts":[{"content":"Terre","features":{"hint":"","pos":"*","features":"*","kigi":"","generated_forms":[{"token":"Terre","pos":"S","features":"sg g"},{"token":"Terre","pos":"S","features":"sg n"},{"token":"Terre+d","pos":"S","features":"pl n"},{"token":"Terre+de","pos":"S","features":"pl g"},{"token":"Terre+dega","pos":"S","features":"pl kom"},{"token":"Terre+deks","pos":"S","features":"pl tr"},{"token":"Terre+del","pos":"S","features":"pl ad"},{"token":"Terre+dele","pos":"S","features":"pl all"},{"token":"Terre+delt","pos":"S","features":"pl abl"},{"token":"Terre+dena","pos":"S","features":"pl es"},{"token":"Terre+deni","pos":"S","features":"pl ter"},{"token":"Terre+des","pos":"S","features":"pl in"},{"token":"Terre+desse","pos":"S","features":"pl ill"},{"token":"Terre+dest","pos":"S","features":"pl el"},{"token":"Terre+deta","pos":"S","features":"pl ab"},{"token":"Terre+ga","pos":"S","features":"sg kom"},{"token":"Terre+ks","pos":"S","features":"sg tr"},{"token":"Terre+l","pos":"S","features":"sg ad"},{"token":"Terre+le","pos":"S","features":"sg all"},{"token":"Terre+lt","pos":"S","features":"sg abl"},{"token":"Terre+na","pos":"S","features":"sg es"},{"token":"Terre+ni","pos":"S","features":"sg ter"},{"token":"Terre+s","pos":"S","features":"sg in"},{"token":"Terre+sid","pos":"S","features":"pl p"},{"token":"Terre+sse","pos":"S","features":"sg ill"},{"token":"Terre+st","pos":"S","features":"sg el"},{"token":"Terre+t","pos":"S","features":"sg p"},{"token":"Terre+ta","pos":"S","features":"sg ab"}]}},{"content":"\u00dcRO","features":{"hint":"","pos":"*","features":"*","kigi":"","generated_forms":[{"token":"\u00dcRO","pos":"Y","features":"?"}]}},{"content":"saadik.","features":{"hint":"","pos":"*","features":"*","kigi":"","generated_forms":[{"token":"saadik","pos":"K","features":""},{"token":"saadik","pos":"S","features":"sg n"},{"token":"saadiku","pos":"S","features":"sg g"},{"token":"saadiku+d","pos":"S","features":"pl n"},{"token":"saadiku+ga","pos":"S","features":"sg kom"},{"token":"saadiku+id","pos":"S","features":"pl p"},{"token":"saadiku+iks","pos":"S","features":"pl tr"},{"token":"saadiku+il","pos":"S","features":"pl ad"},{"token":"saadiku+ile","pos":"S","features":"pl all"},{"token":"saadiku+ilt","pos":"S","features":"pl abl"},{"token":"saadiku+ina","pos":"S","features":"pl es"},{"token":"saadiku+ini","pos":"S","features":"pl ter"},{"token":"saadiku+is","pos":"S","features":"pl in"},{"token":"saadiku+isse","pos":"S","features":"pl ill"},{"token":"saadiku+ist","pos":"S","features":"pl el"},{"token":"saadiku+ks","pos":"S","features":"sg tr"},{"token":"saadiku+l","pos":"S","features":"sg ad"},{"token":"saadiku+le","pos":"S","features":"sg all"},{"token":"saadiku+lt","pos":"S","features":"sg abl"},{"token":"saadiku+na","pos":"S","features":"sg es"},{"token":"saadiku+ni","pos":"S","features":"sg ter"},{"token":"saadiku+s","pos":"S","features":"sg in"},{"token":"saadiku+sse","pos":"S","features":"sg ill"},{"token":"saadiku+st","pos":"S","features":"sg el"},{"token":"saadiku+t","pos":"S","features":"sg p"},{"token":"saadiku+ta","pos":"S","features":"sg ab"},{"token":"saadiku+te","pos":"S","features":"pl g"},{"token":"saadiku+tega","pos":"S","features":"pl kom"},{"token":"saadiku+teks","pos":"S","features":"pl tr"},{"token":"saadiku+tel","pos":"S","features":"pl ad"},{"token":"saadiku+tele","pos":"S","features":"pl all"},{"token":"saadiku+telt","pos":"S","features":"pl abl"},{"token":"saadiku+tena","pos":"S","features":"pl es"},{"token":"saadiku+teni","pos":"S","features":"pl ter"},{"token":"saadiku+tes","pos":"S","features":"pl in"},{"token":"saadiku+tesse","pos":"S","features":"pl ill"},{"token":"saadiku+test","pos":"S","features":"pl el"},{"token":"saadiku+teta","pos":"S","features":"pl ab"}]}}]}}
```

### Example 2.

Only the specified inflectional forms will be generated.

```commandline
curl --silent --request POST --header "Content-Type: application/json" --data '{"type":"structuredText", "texts":[{"content":"saadik", "features":{"hint":"", "pos":"S", "features":"sg kom", "kigi":""}}]}' localhost:7000/process | jq
```

```json
{
  "response": {
    "type": "texts",
    "texts": [
      {
        "content": "saadik",
        "features": {
          "hint": "",
          "pos": "S",
          "features": "sg kom",
          "kigi": "",
          "generated_forms": [
            {
              "token": "saadiku+ga",
              "pos": "S",
              "features": "sg kom"
            }
          ]
        }
      }
    ]
  }
}

```

### Example 3.

Only the specified inflectional forms will be generated.

```commandline
curl --silent --request POST --header "Content-Type: application/json" --data '{"params":{"cmdline":["--guess"]}, "type":"structuredText", "texts":[{"content":"palk", "features":{"hint":"", "pos":"S", "features":"sg kom", "kigi":""}}]}' localhost:7000/process | jq
```

```json
{
  "response": {
    "type": "texts",
    "texts": [
      {
        "content": "palk",
        "features": {
          "hint": "",
          "pos": "S",
          "features": "sg kom",
          "kigi": "",
          "generated_forms": [
            {
              "token": "palga+ga",
              "pos": "S",
              "features": "sg kom"
            },
            {
              "token": "palgi+ga",
              "pos": "S",
              "features": "sg kom"
            }
          ]
        }
      }
    ]
  }
}
```

### Example 4.

Only the specified inflectional forms will be generated, and the forms will contain special characters for pronunciation.

```commandline
curl --silent --request POST --header "Content-Type: application/json" --data '{"params":{"cmdline":["--addphonetics"]}, "type":"structuredText", "texts":[{"content":"palk", "features":{"hint":"palgi", "pos":"S", "features":"sg kom", "kigi":""}}]}' localhost:7000/process | jq
```

```json
{
  "response": {
    "type": "texts",
    "texts": [
      {
        "content": "palk",
        "features": {
          "hint": "palgi",
          "pos": "S",
          "features": "sg kom",
          "kigi": "",
          "generated_forms": [
            {
              "token": "pal]gi+ga",
              "pos": "S",
              "features": "sg kom"
            }
          ]
        }
      }
    ]
  }
}
```

## See also

* [Command line version of the morphological generator for Estonian](https://github.com/Filosoft/vabamorf/blob/master/apps/cmdline/vmets/)

## Sponsors

The container development was sponsored by EU CEF project [Microservices at your service](https://www.lingsoft.fi/en/microservices-at-your-service-bridging-gap-between-nlp-research-and-industry)


## Authors

Authors of the container: Tarmo Vaino, Heiki-Jaan Kaalep

Authors of the contents of the container: see references at section [Contains](#Contains).
 
