# Eesti keele morfoloogilise süntesaatori konteiner

[Filosofti morfoloogilise sünteesi programmi](https://github.com/Filosoft/vabamorf/blob/master/apps/cmdline/vmets/LOEMIND.md) sisaldav tarkvara-konteiner (docker), mille liides vastab [ELG nõuetele](https://european-language-grid.readthedocs.io/en/release1.0.0/all/LTInternalAPI.html#).

## Mida sisaldab <a name="Mida_sisaldab"></a>

* [Filosofti morfoloogilise sünteesi programm](https://github.com/Filosoft/vabamorf/blob/master/apps/cmdline/vmets/LOEMIND.md) 
* Konteineri ja liidesega seotud lähtekood

## Eeltingimused

* Peab olema paigaldatud tarkvara konteineri tegemiseks/kasutamiseks; juhised on [docker'i veebilehel](https://docs.docker.com/).
* Kui sooviks on lähtekoodi ise kompileerida või konteinerit kokku panna, siis peab olema paigaldatud versioonihaldustarkvara; juhised on [git'i veebilehel](https://git-scm.com/).

## Konteineri allalaadimine Docker Hub'ist

Valmis konteineri saab laadida alla Docker Hub'ist, kasutades Linux'i käsurida (Windows'i/Mac'i käsurida on analoogiline):

```commandline
docker pull tilluteenused/vabamorf_synth:2022.08.15
```

Seejärel saab jätkata osaga [Konteineri käivitamine](#Konteineri_käivitamine).

## Ise konteineri tegemine

### 1. Lähtekoodi allalaadimine 

<!---
Lähtekood koosneb 2 osast
1. json liides, veebiserver ja konteineri tegemise asjad
2. FSi morf sünteesi programm
---->

```commandline
mkdir -p ~/gitlab-docker-elg
cd ~/gitlab-docker-elg
git clone https://gitlab.com/tilluteenused/docker-elg-synth gitlab-docker-elg-synth
```

Repositoorium sisaldab kompileeritud [Filosofti morfoloogilise sünteesi programmi](https://github.com/Filosoft/vabamorf/blob/master/apps/cmdline/vmets/LOEMIND.md)
ja andmefaile:

* **_vmets_** morfoloogilise sünteesi programm.
* **_et.dct_** programmi poolt kasutatav leksikon.

Kui soovite ise programmi (**_vmets_**) kompileerida või leksikoni (**_et.dct_**) täiendada/muuta ja uuesti kokku panna, 
vaadake sellekohast [juhendit](https://github.com/Filosoft/vabamorf/blob/master/doc/programmid_ja_sonastikud.md).

### 2. Konteineri kokkupanemine

```commandline
cd ~/gitlab-docker-elg/gitlab-docker-elg-synth
docker build -t tilluteenused/vabamorf_synth:2022.08.15 .
```

## Konteineri käivitamine <a name="Konteineri_käivitamine"></a>

```commandline
docker run -p 7000:7000 tilluteenused/vabamorf_synth:2022.08.15
```

Pole oluline, milline on jooksev kataloog terminaliaknas konteineri käivitamise hetkel.

Käivitatud konteineri töö lõpetab Ctrl+C selles terminaliaknas, kust konteiner käivitati. 

## Päringu json-kuju

Tasub tähele panna, et Python'i json'i teek esitab teksti vaikimisi ASCII kooditabelis; 
täpitähed jms esitatakse Unicode'i koodidena, nt. õ = \u00f5.

### Variant 1.

Sõnadele sünteesitakse nende kõik võimalikud vormid.

```json
{
  "type":"text",
  "content": string, /* Tühikuga eraldatud algvormide e. LEMMAde loend. Igast LEMMAst sünteesitakse kõik võimalikud vormid */
}
```

### Variant 2.

Sõnadele sünteesitakse ainult nõutavad vormid.

```json
{
  "params":{"cmdline": []}   /* LIPUD; pole kohustuslik */
  "type": "structuredText",
  "texts": [ 
    {
      "content": string /* sünteesitava sõna algvorm e. LEMMA */
      "features": 
      {
        "hint": string,     /* NÄIDIS; vaikimisi on tühi string */
        "pos": string,      /* SÕNALIIK; '*' tähistab kõikvõimalikke sõnaliike */
        "features": string, /* VORM; '*' tähistab kõikvõimalikke vorme */
        "kigi": string      /* KIGI LIIDE; vaikimisi on tühi string */
      }
    }
  ]
}
```

## Vastuse json-kuju

Vastuses sisaldub iga sõna puhul info ka selle kohta, milline oli päring.

```json
{ "response":
  { "type":"texts",
    "texts":
    [ { "content": string, /* sünteesitava sõna algvorm e. LEMMA */
        "features":
        { "hint": string,     /* sünteesitava NÄIDIS */
          "pos": string,      /* sünteesitava SÕNALIIK */
          "features": string, /* sünteesitava VORM */
          "kigi":string       /* sünteesitava KIGI LIIDE */
          "generated_forms":  /* genereeritud vormid */
          [ { "token": string,    /* sünteesitud sõnavorm */
              "pos":string,       /* sünteesitud sõna SÕNALIIK */
              "features": string  /* sünteesitud sõna VORM (kääne/pööre jms) */
            }
          ]
        }
      }
    ]
  }
}    
```

JSON struktuuri elementidele kommentaarina lisatud sõnad näitavad vastavust sünteesiprogrammi sisendi ja väljundiga, nagu neid käsitleb [morfoloogilise sünteesi programmi kirjeldus](https://github.com/Filosoft/vabamorf/blob/master/apps/cmdline/vmets/LOEMIND.md):
params = LIPUD, content = LEMMA, hint = NÄIDIS, pos = SÕNALIIK, features = VORM

Lisaks on siin kasutusel element ```kigi``` , mis tähistab kliitikut ki/gi:

* _"kigi": "kigi"_ korral liidetakse sünteesitud sõnale _ki_ või _gi_ liide (kumb sobib)
* _"kigi": ""_ korral _ki_ või _gi_ liidet ei lisata

## Kasutusnäited

### Näide 1.

Sünteesitakse kõik võimalikud vormid.

```commandline
curl -i --request POST --header "Content-Type: application/json" --data '{"type": "text", "content": "Terre ÜRO saadik."}' localhost:7000/process
```

```json
HTTP/1.1 200 OK
Server: gunicorn
Date: Mon, 15 Aug 2022 14:09:38 GMT
Connection: close
Content-Type: application/json
Content-Length: 3796

{"response":{"type":"texts","texts":[{"content":"Terre","features":{"hint":"","pos":"*","features":"*","kigi":"","generated_forms":[{"token":"Terre","pos":"S","features":"sg g"},{"token":"Terre","pos":"S","features":"sg n"},{"token":"Terre+d","pos":"S","features":"pl n"},{"token":"Terre+de","pos":"S","features":"pl g"},{"token":"Terre+dega","pos":"S","features":"pl kom"},{"token":"Terre+deks","pos":"S","features":"pl tr"},{"token":"Terre+del","pos":"S","features":"pl ad"},{"token":"Terre+dele","pos":"S","features":"pl all"},{"token":"Terre+delt","pos":"S","features":"pl abl"},{"token":"Terre+dena","pos":"S","features":"pl es"},{"token":"Terre+deni","pos":"S","features":"pl ter"},{"token":"Terre+des","pos":"S","features":"pl in"},{"token":"Terre+desse","pos":"S","features":"pl ill"},{"token":"Terre+dest","pos":"S","features":"pl el"},{"token":"Terre+deta","pos":"S","features":"pl ab"},{"token":"Terre+ga","pos":"S","features":"sg kom"},{"token":"Terre+ks","pos":"S","features":"sg tr"},{"token":"Terre+l","pos":"S","features":"sg ad"},{"token":"Terre+le","pos":"S","features":"sg all"},{"token":"Terre+lt","pos":"S","features":"sg abl"},{"token":"Terre+na","pos":"S","features":"sg es"},{"token":"Terre+ni","pos":"S","features":"sg ter"},{"token":"Terre+s","pos":"S","features":"sg in"},{"token":"Terre+sid","pos":"S","features":"pl p"},{"token":"Terre+sse","pos":"S","features":"sg ill"},{"token":"Terre+st","pos":"S","features":"sg el"},{"token":"Terre+t","pos":"S","features":"sg p"},{"token":"Terre+ta","pos":"S","features":"sg ab"}]}},{"content":"\u00dcRO","features":{"hint":"","pos":"*","features":"*","kigi":"","generated_forms":[{"token":"\u00dcRO","pos":"Y","features":"?"}]}},{"content":"saadik.","features":{"hint":"","pos":"*","features":"*","kigi":"","generated_forms":[{"token":"saadik","pos":"K","features":""},{"token":"saadik","pos":"S","features":"sg n"},{"token":"saadiku","pos":"S","features":"sg g"},{"token":"saadiku+d","pos":"S","features":"pl n"},{"token":"saadiku+ga","pos":"S","features":"sg kom"},{"token":"saadiku+id","pos":"S","features":"pl p"},{"token":"saadiku+iks","pos":"S","features":"pl tr"},{"token":"saadiku+il","pos":"S","features":"pl ad"},{"token":"saadiku+ile","pos":"S","features":"pl all"},{"token":"saadiku+ilt","pos":"S","features":"pl abl"},{"token":"saadiku+ina","pos":"S","features":"pl es"},{"token":"saadiku+ini","pos":"S","features":"pl ter"},{"token":"saadiku+is","pos":"S","features":"pl in"},{"token":"saadiku+isse","pos":"S","features":"pl ill"},{"token":"saadiku+ist","pos":"S","features":"pl el"},{"token":"saadiku+ks","pos":"S","features":"sg tr"},{"token":"saadiku+l","pos":"S","features":"sg ad"},{"token":"saadiku+le","pos":"S","features":"sg all"},{"token":"saadiku+lt","pos":"S","features":"sg abl"},{"token":"saadiku+na","pos":"S","features":"sg es"},{"token":"saadiku+ni","pos":"S","features":"sg ter"},{"token":"saadiku+s","pos":"S","features":"sg in"},{"token":"saadiku+sse","pos":"S","features":"sg ill"},{"token":"saadiku+st","pos":"S","features":"sg el"},{"token":"saadiku+t","pos":"S","features":"sg p"},{"token":"saadiku+ta","pos":"S","features":"sg ab"},{"token":"saadiku+te","pos":"S","features":"pl g"},{"token":"saadiku+tega","pos":"S","features":"pl kom"},{"token":"saadiku+teks","pos":"S","features":"pl tr"},{"token":"saadiku+tel","pos":"S","features":"pl ad"},{"token":"saadiku+tele","pos":"S","features":"pl all"},{"token":"saadiku+telt","pos":"S","features":"pl abl"},{"token":"saadiku+tena","pos":"S","features":"pl es"},{"token":"saadiku+teni","pos":"S","features":"pl ter"},{"token":"saadiku+tes","pos":"S","features":"pl in"},{"token":"saadiku+tesse","pos":"S","features":"pl ill"},{"token":"saadiku+test","pos":"S","features":"pl el"},{"token":"saadiku+teta","pos":"S","features":"pl ab"}]}}]}}
```

### Näide 2.

Sünteesitakse ainult nõutavad vormid.

```commandline
curl --silent --request POST --header "Content-Type: application/json" --data '{"type":"structuredText", "texts":[{"content":"saadik", "features":{"hint":"", "pos":"S", "features":"sg kom", "kigi":""}}]}' localhost:7000/process | jq
```

```json
{
  "response": {
    "type": "texts",
    "texts": [
      {
        "content": "saadik",
        "features": {
          "hint": "",
          "pos": "S",
          "features": "sg kom",
          "kigi": "",
          "generated_forms": [
            {
              "token": "saadiku+ga",
              "pos": "S",
              "features": "sg kom"
            }
          ]
        }
      }
    ]
  }
}

```

### Näide 3.

Sünteesitakse ainult nõutavad vormid.

```commandline
curl --silent --request POST --header "Content-Type: application/json" --data '{"params":{"cmdline":["--guess"]}, "type":"structuredText", "texts":[{"content":"palk", "features":{"hint":"", "pos":"S", "features":"sg kom", "kigi":""}}]}' localhost:7000/process | jq
```

```json
{
  "response": {
    "type": "texts",
    "texts": [
      {
        "content": "palk",
        "features": {
          "hint": "",
          "pos": "S",
          "features": "sg kom",
          "kigi": "",
          "generated_forms": [
            {
              "token": "palga+ga",
              "pos": "S",
              "features": "sg kom"
            },
            {
              "token": "palgi+ga",
              "pos": "S",
              "features": "sg kom"
            }
          ]
        }
      }
    ]
  }
}
```

### Näide 4

Sünteesitakse ainult nõutavad vormid koos hääldusmärkidega.

```commandline
curl --silent --request POST --header "Content-Type: application/json" --data '{"params":{"cmdline":["--addphonetics"]}, "type":"structuredText", "texts":[{"content":"palk", "features":{"hint":"palgi", "pos":"S", "features":"sg kom", "kigi":""}}]}' localhost:7000/process | jq
```

```json
{
  "response": {
    "type": "texts",
    "texts": [
      {
        "content": "palk",
        "features": {
          "hint": "palgi",
          "pos": "S",
          "features": "sg kom",
          "kigi": "",
          "generated_forms": [
            {
              "token": "pal]gi+ga",
              "pos": "S",
              "features": "sg kom"
            }
          ]
        }
      }
    ]
  }
}
```

## Vaata lisaks

[Eesti keele morfoloogilise sünteesi käsureaprogramm](https://github.com/Filosoft/vabamorf/blob/master/apps/cmdline/vmets/LOEMIND.md)

## Rahastus

Konteiner loodi EL projekti [Microservices at your service](https://www.lingsoft.fi/en/microservices-at-your-service-bridging-gap-between-nlp-research-and-industry) toel.

## Autorid

Konteineri autorid: Tarmo Vaino, Heiki-Jaan Kaalep

Konteineri sisu autoreid vt. jaotises [Mida sisaldab](#Mida_sisaldab) toodud viidetest.


